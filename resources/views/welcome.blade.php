<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Laravel 6 with Docker</title>
</head>
<body>
  <h2>Product List</h2>
  @foreach ($data as $item)
    <p>{{ $item->name}}</p>
  @endforeach
</body>
</html>
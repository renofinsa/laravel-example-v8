<?php

use Illuminate\Database\Seeder;
use App\Product;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        Product::create([
          'name' => 'Banana'
        ]);

        Product::create([
          'name' => 'Orange'
        ]);

        Product::create([
          'name' => 'Guava'
        ]);

        Product::create([
          'name' => 'Mango'
        ]);

        Product::create([
          'name' => 'Watermelon'
        ]);
    }
}
